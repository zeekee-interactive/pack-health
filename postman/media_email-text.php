Plain Text

Name of the Event Requested: <?=$aOutput['eventrequested'] ?>
Person Requesting Credentials: <?=$aOutput['person'] ?>
Title: <?=$aOutput['title'] ?>
Business Phone: <?=$aOutput['phone'] ?>
Cell Phone: <?=$aOutput['cell'] ?>
Email: <?=$aOutput['email'] ?>
Name of Organization: <?=$aOutput['organization'] ?>
Address: <?=$aOutput['address'] ?>
City: <?=$aOutput['city'] ?>
State: <?=$aOutput['state'] ?>
Zip Code: <?=$aOutput['zip'] ?>
Organization Website: <?=$aOutput['website'] ?>
Staff covering the event: <?=nl2br( $aOutput['staff-info'] ) ?>
Staff emails: <?=nl2br( $aOutput['staff-emails'] ) ?>
Have you ever covered this event before? <?=$aOutput['covered1'] ?>
If yes, which year? <?=$aOutput['covered2'] ?>
Position of staff covering the event: <?=$aOutput['position'] ?>
If more than one, please list the name and position of each staff member: <?=nl2br( $aOutput['staff_positions'] ) ?>
# of Seats in Media Center Requested: <?=$aOutput['numberseats'] ?>
# of parking Passes requested: <?=$aOutput['numberpasses'] ?>
Date of Publication or Air Date (mm/dd/yy): <?=$aOutput['datepub'] ?>
Position of staff covering the event: <?=$aOutput['category'] ?>
If you chose Other please explain your category:  <?=nl2br( $aOutput['othercat'] ) ?>
Comments and additional Information: <?=nl2br( $aOutput['comments'] ) ?>

This does not have the same fields and is to show that you can have different
text. In each version of the email.