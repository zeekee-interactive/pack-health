HTML Version<p />
<table>
	<tr>
		<td width="25%">Name of the Event Requested:</td>
		<td width="75%"><?=$aOutput['eventrequested'] ?></td>
	</tr>
	<tr>
		<td>Person Requesting Credentials:</td>
		<td><?=$aOutput['person'] ?></td>
	</tr>
	<tr>
		<td>Title:</td>
		<td><?=$aOutput['title'] ?></td>
	</tr>
	<tr>
		<td>Business Phone:</td>
		<td><?=$aOutput['phone'] ?></td>
	</tr>
	<tr>
		<td>Cell Phone:</td>
		<td><?=$aOutput['cell'] ?></td>
	</tr>
	<tr>
		<td>Email:</td>
		<td><?=$aOutput['email'] ?></td>
	</tr>
	<tr>
		<td>Name of Organization:</td>
		<td><?=$aOutput['organization'] ?></td>
	</tr>
	<tr>
		<td>Address:</td>
		<td><?=$aOutput['address'] ?></td>
	</tr>
	<tr>
		<td>City:</td>
		<td><?=$aOutput['city'] ?></td>
	</tr>
	<tr>
		<td>State:</td>
		<td><?=$aOutput['state'] ?></td>
	</tr>
	<tr>
		<td>Zip Code:</td>
		<td><?=$aOutput['zip'] ?></td>
	</tr>
	<tr>
		<td>Organization Website:</td>
		<td><?=$aOutput['website'] ?></td>
	</tr>
	<tr>
		<td>Staff covering the event:</td>
		<td><?=nl2br( $aOutput['staff-info'] ) ?></td>
	</tr>
	<tr>
		<td>Staff emails:</td>
		<td><?=nl2br( $aOutput['staff-emails'] ) ?></td>
	</tr>
	<tr>
		<td>Have you ever covered this event before?</td>
		<td><?=$aOutput['covered1'] ?></td>
	</tr>
	<tr>
		<td>If yes, which year?</td>
		<td><?=$aOutput['covered2'] ?></td>
	</tr>
	<tr>
		<td>Position of staff covering the event:</td>
		<td><?=$aOutput['position'] ?></td>
	</tr>
	<tr>
		<td>If more than one, please list the name and position of each staff member:</td>
		<td><?=nl2br( $aOutput['staff_positions'] ) ?></td>
	</tr>
	<tr>
		<td># of Seats in Media Center Requested:</td>
		<td><?=$aOutput['numberseats'] ?></td>
	</tr>
	<tr>
		<td># of parking Passes requested:</td>
		<td><?=$aOutput['numberpasses'] ?></td>
	</tr>
	<tr>
		<td>Date of Publication or Air Date (mm/dd/yy):</td>
		<td><?=$aOutput['datepub'] ?></td>
	</tr>
	<tr>
		<td>Position of staff covering the event:</td>
		<td><?=$aOutput['category'] ?></td>
	</tr>
	<tr>
		<td>If you chose Other please explain your category: </td>
		<td><?=nl2br( $aOutput['othercat'] ) ?></td>
	</tr>
	<tr>
		<td>Comments and additional Information:</td>
		<td><?=nl2br( $aOutput['comments'] ) ?></td>
	</tr>
</table>