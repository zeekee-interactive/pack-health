<?php
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'About';
	$cSEOTitle = '';
	$layout = 'home';
?>

<?php
	include("header.php");
?>

<div class="main-container">

	<section class="subpage-blue">
		<h1>Got a Pack? <span class="header-icon"><img src="img/packs-icon-blue.png" alt="packs-icon-blue"</span></h1>

		<div class="row">
			<div class="medium-8 columns">
				<form>
					<div class="row">
						<div class="medium-12 columns">
							<label>Address</label>
							<input type="text">
						</div>
					</div>
					<div class="row">
						<div class="medium-4 columns">
							<label>Phone</label>
							<input type="text">
						</div>
					</div>
					<div class="row">
						<div class="medium-12 columns">
							<label>Email</label>
							<input type="text">
						</div>
					</div>
				</form>
				<a class="button section-push" href="packs-patients-confirm.php">Next</a>
			</div>
			<div class="medium-4 columns">
				<img src="img/request-2.jpg" class="th" alt="request-2" width="800" height="533" />
			</div>
		</div><!-- /row -->
	</section>
	
</div><!--END TABLET AND DESKTOP UP ONLY-->

<?php
	include("footer.php");
?>