<?php
	include($_SERVER['DOCUMENT_ROOT']. '/tyfoon/connect.php');
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'Prescribers';
	$cSEOTitle = '';
	$layout = 'home';
?>

<?php
	include("header.php");

?>

<div class="main-container">

	<section class="subpage-blue">
		<h1>For Prescribers <span class="header-icon"><img src="img/packs-icon-blue.png" alt="packs-icon-blue"</span></h1>

		<div class="row">
			<div class="large-3 columns">
				<h3>Who We Serve</h3>
			</div>
			<div class="large-9 columns">
				<h3>What We Do</h3>
			</div>
		</div><!-- /.row -->

		<div class="section-push"></div>
		<div class="row">
			<div class="large-3 columns">
				<h3 class="has-icon"><a href="/pharmacos.php">PharmaCos</a></h3>
			</div>
			<div class="large-9 columns">
				<p>Build your "beyond the pill" strategy</p>
			</div>
		</div><!-- /.row -->

		<div class="row">
			<div class="large-3 columns">
				<h3 class="has-icon"><a href="/providers.php">Providers</a></h3>
			</div>
			<div class="large-9 columns">
				<p>Prescribe change to critical patients<br>
					Innovate at limited cost<br>
					Target today's metrics of success
				</p>
			</div>
		</div><!-- /.row -->

		<div class="row">
			<div class="large-3 columns">
				<h3 class="has-icon"><a href="/payers.php">Payers</a></h3>
			</div>
			<div class="large-9 columns">
				<p>Implement cost-effective innovations<br>
					Target HEDIS and Medicare Stars
				</p>
			</div>
		</div><!-- /.row -->

		<div class="row">
			<div class="large-3 columns">
				<h3 class="has-icon"><a href="/caregivers.php">Caregivers</a></h3>
			</div>
			<div class="large-9 columns">
				<p>Help your loved ones manage their health<br>
					Organize tools and education for your loved ones
				</p>
			</div>
		</div><!-- /.row -->
	</section>

</div><!--END TABLET AND DESKTOP UP ONLY-->

<?php
	include("footer.php");
?>
