<?php
	include($_SERVER['DOCUMENT_ROOT']. '/tyfoon/connect.php');
	$aPage = pageGet(65);
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'Home';
	$cSEOTitle = '';
	$layout = 'home';
?>

<?php
	include("header.php");
?>


<!--BEGIN DESKTOP SLIDER SECTION-->

<div class="main-container"><!--TABLET AND DESKTOP UP ONLY-->
	<section class="icons">
		<div class="row">
			<div class="medium-10 columns small-10 small-centered">
				<img src="img/PACKHEALTH_LOGO_Horizontal_CMYK.png" alt="PACKHEALTH_LOGO_Horizontal_CMYK" class="pack-logo-index" />
			</div>
		</div>
		<div class="row">
			<div class="medium-12 columns medium-centered">
				<p class="text-center"><span style="font-size:190%;">OUR PACK PROGRAM PUTS YOU ON THE PATH TO BETTER HEALTH.</span></p>
				<p class="text-center"><span style="font-size:300%;">GOT A PRESCRIPTION?</span></p>
				<!--<p class="text-center">BE HEALTHIER. OUR PACK PROGRAM PUTS YOU ON THE PATH TO BETTER HEALTH.</p>-->
				<!--<p class="text-center">WE ARE A COMPANY BASED IN BIRMINGHAM, ALABAMA WHO STARTED WITH A SIMPLE QUESTION: HOW CAN WE ENGAGE PATIENTS TO BETTER MANAGE THEIR HEALTHCARE? OUR GOALS ARE SIMPLE: TO EDUCATE AND MOTIVATE PATIENTS TO BEST MANAGE CHRONIC CONDITIONS, AND TO SAVE HOSPITALS AND HEALTHCARE SYSTEMS MONEY IN THE PROCESS. WE BELIEVE THAT EVERY PATIENT IS AN INDIVIDUAL, AND SHOULD BE TREATED AS SO. WE BELIEVE IN THE PERSONALIZATION OF MANAGEMENT OF CHRONIC DISEASE.</p>-->
			</div>
		</div>
		<div class="row">
			<!--<div class="medium-10 columns small-12 small-centered cta-main">
				<div class="row">
					<div class="medium-8 columns medium-centered">
						<a class="button expand" href="patients.php"><span style="font-size:225%;">Got a prescription?</span></a>
					</div>
				</div>-->
			<div class="medium-10 columns small-12 small-centered cta-main">
				<div class="row">
					<div class="medium-6 columns medium-centered">
						<a class="button expand" href="patients.php"><span style="font-size:250%;">Start Here</span></a>
					</div>
				</div>
			</div>		
			<!--<div class="medium-8 columns small-10 small-centered cta-main">
				<p>Got a prescription?</p>
				<div class="row">
					<div class="medium-8 columns medium-centered">
						<a class="button expand" href="patients.php"><span style="font-size:125%;">Start Your Program Here</span></a>
					</div>
				</div>
			</div>-->
		</div>
		<!--<div class="row">
			<div class="medium-8 columns small-10 small-centered cta-main">
				<div class="row">
					<div class="medium-5 columns medium-centered">
						<a href="patients.php"><img src="img/icons/im-patient-icon-index.png" alt="im a patient" /></a>
					</div>
				</div>
			</div>
			<!--<div class="medium-10 columns small-centered small-8">
				<ul class="medium-block-grid-3">
					<li><a href="patients.php"><img src="img/icons/im-patient-icon-index.png" alt="im a patient" /></a></li>
					<li><a href="prescribers.php"><img src="img/icons/im-prescriber-icon-index.png" alt="im a prescriber" /></a></li>
					<li><a href="packs-overview.php"><img src="img/icons/been-prescribed-icon-index.png" alt="ive been prescribed a pack" /></a></li>
				</ul>
			</div>-->
		<!--</div>-->
		<!--<div class="row">
			</div>			
			<div class="medium-8 columns small-10 small-centered cta-main">
				<p>Don't have a prescription?</p>
				<div class="row">
					<div class="medium-6 columns medium-centered">
						<a class="button expand" href="packs-overview.php">Learn More About Pack Health</a>
					</div>
				</div>
			</div>
		</div>-->
	</section>

</div><!--END TABLET AND DESKTOP UP ONLY-->

<?php
	include("footer.php");
?>
