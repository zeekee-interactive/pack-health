<!-- START OFFCANVAS MOBILE NAV -->
<div class="off-canvas-wrap hide-for-medium-up" data-offcanvas>
  <div class="inner-wrap">
    <nav class="tab-bar">
      <section class="left-small">
        <a class="left-off-canvas-toggle menu-icon" href="#"><span></span><img src="img/pack-logo.png" alt="pack-logo" /></a>
      </section>
    </nav>

    <aside class="left-off-canvas-menu">
      <ul class="off-canvas-list">
        <li><label>Pack Health</label></li>
        <li><a href="index.php"><img src="img/home-img.png" alt="home-img" />
			Home
		</a></li>
        <li><a href="#"><img src="img/packs-icon.png" alt="packs-icon" />
			Packs</a>
        	<ul>
	        	<li><a href="">For Patients</a></li>
	        	<li><a href="">For Referrers</a></li>
        	</ul>
        </li>
        <li><a href="about.php"><img src="img/about-icon.png" alt="about-icon" />
        	About Us
        </a></li>
        <li><a href="#"><img src="img/news-icon.png" alt="news-icon" />
        	News
        </a></li>
        <li><a href="#"><img src="img/contact-icon.png" alt="contact-icon" />
        	Contact Us
        </a></li>
      </ul>
    </aside>

    <section class="main-section"><!--MAIN CONTENT AREA MOBILE-->
		
		<div class="main-container">
			

			<section class="patient-refer">
				<div class="row">
					<div class="large-12 columns">
						<h2>WELCOME</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sit amet lorem et est ultricies tristique dictum in massa. Sed id turpis sit amet mi elementum sodales. Nam semper neque vel urna ultrices iaculis. Fusce non sodales mi. Suspendisse sed mauris quis est congue auctor. Nam tempus fermentum nisl, et tincidunt neque lacinia at. <a href="">Phasellus ut lacinia nisl, in rutrum risus.</a></p>
					</div>
				</div>
				<div class="row">
					<div class="large-12 columns">
						<img src="img/open-pack.jpg" alt="open-pack" />
					</div>
				</div>
				<div class="row">
					<div class="medium-6 columns">
						<article>
							<img src="img/for-patients-icon.png" alt="for-patients-icon" width="49" height="47" />
							<h2>For patients</h2>
							<p>Praesent posuere viverra tellus vitae condimentum. Nam metus nisi, commodo in neque et, suscipit faucibus ligula. Cras sit amet dolor a est semper iaculis.</p>
							<a href="">
								<div class="circle">
									<h3>Get your<br> pack</h3>
								</div>
							</a>
						</article>
					</div>
					<div class="medium-6 columns">
						<article>
							<img src="img/for-referrers-icon.png" alt="for-referrers-icon" width="59" height="53" />
							<h2>For referrers</h2>
							<p>Praesent posuere viverra tellus vitae condimentum. Nam metus nisi, commodo in neque et, suscipit faucibus ligula. Cras sit amet dolor a est semper iaculis.</p>
							<a href="">
								<div class="circle">
									<h3>Learn more about packs</h3>
								</div>
							</a>
						</article>
					</div>
				</div>
			</section>
		</div>
		
    </section><!--ENDS MAIN CONTENT AREA MOBILE-->

  <a class="exit-off-canvas"></a>

  </div>
</div>