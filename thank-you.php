<?php
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'Thank You';
	$cSEOTitle = '';
	$layout = 'home';
?>

<?php
	include("header.php");
?>

<div class="main-container">


	<section class="subpage-blue">
		<h1>Thank You for Contacting Us <span class="header-icon"><img src="img/contact-icon-blue.png" alt="about-us-icon"</span></h1>
		<div class="row">
			<div class="large-8 columns">
				
				<p>We will contact you shortly.</p>
				<a class="button" href="index.php">Return to Home</a>
				
				
			</div>
		</div>
	</section>

</div><!--END TABLET AND DESKTOP UP ONLY-->

<?php
	include("footer.php");
?>