<?php
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'About';
	$cSEOTitle = '';
	$layout = 'home';
?>

<?php
	include("header.php");
?>

<div class="main-container">

	<section class="subpage-blue">
		<h1>Please confirm your information <span class="header-icon"><img src="img/packs-icon-blue.png" alt="packs-icon-blue"</span></h1>

		<div class="row">
			<div class="medium-6 columns">
				
				<div class="row">
					<div class="medium-8 columns">
						<p>Name: </p>
					</div>
				</div>
				<div class="row">
					<div class="medium-8 columns">
						<p>Unique Code: </p>
					</div>
				</div>
			</div>
			<div class="medium-6 columns">
				<div class="row">
					<div class="medium-8 columns">
						<p>Address: </p>
					</div>
				</div>
				<div class="row">
					<div class="medium-8 columns">
						<p>Phone: </p>
					</div>
				</div>
				<div class="row">
					<div class="medium-8 columns">
						<p>Email: </p>
					</div>
				</div>
			</div>
		</div> <!-- / row -->
		
		<div class="row">
			<div class="medium-8 columns medium-centered">
				<div class="row">
					<div class="medium-6 columns">
						<a class="button expand" href="packs-patients.php">Edit</a>
					</div>
					<div class="medium-6 columns">
						<a class="button expand">Confirm</a>
					</div>
				</div>
			</div>
		</div>
		<div class="section-push"></div>
	</section>
	
</div><!--END TABLET AND DESKTOP UP ONLY-->

<?php
	include("footer.php");
?>