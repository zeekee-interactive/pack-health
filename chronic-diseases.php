<?php
	include($_SERVER['DOCUMENT_ROOT']. '/tyfoon/connect.php');
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'Chronic Diseases';
	$cSEOTitle = '';
	$layout = 'home';
?>

<?php
	include("header.php");

?>

<div class="main-container">

	<section class="subpage-blue">
		<h1>Chronic Diseases <span class="header-icon"><img src="img/packs-icon-blue.png" alt="packs-icon-blue"</span></h1>

		<div class="row">
			<div class="medium-4 columns">
				<h3 class="text-center">2014 pipeline</h3>
				<ul class="pipeline-2014">
					<li>Type 2 Diabetes</li>
					<li>Poly-chronic</li>
					<li>Readmissions</li>
					<li>Hepatitis C Virus</li>
					<li>Cancer Symptoms</li>
					<li>RA</li>
					<li>IBS</li>
					<li>Obesity</li>
					<li>Asthma</li>
					<li>COPD</li>
				</ul>
			</div>
			<div class="medium-4 columns with-arrow">
				<h3 class="text-center">2015 pipeline</h3>
				<div class="gray-box">
					<p>
						All 48 BPCI*** Episodes
					</p>
				</div>
				<p>
					Products for different patient segments
				</p>
				<ul>
					<li>Spanish</li>
					<li>Activation level</li>
					<li>Culture</li>
					<li>Technological aptitude</li>
				</ul>
				<span class="arrow-large"></span>
			</div>
			<div class="medium-4 columns">
				<h3 class="text-center">2016 target</h3>
				<p class="button">1,000,000 patients engaged</p>
			</div>
		</div><!-- /.row -->
	</section>

</div><!--END TABLET AND DESKTOP UP ONLY-->

<?php
	include("footer.php");
?>
