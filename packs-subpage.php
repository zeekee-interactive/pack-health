<?php
	$cMetaDesc = $aOutput['description'];
	$cMetaKW = $aOutput['keywords'];
	$cPageTitle = $aOutput['title'];
	$cSEOTitle = $aOutput['abstract'];
	$layout = 'home';
?>

<?php
	include("header.php");
?>

<div class="main-container">


	<section class="subpage-blue">
		<h1><?php echo $aOutput['title']; ?> <span class="header-icon"><img src="img/packs-icon-blue.png" alt="packs-icon-blue"></span></h1>
		<div class="row">
			<div class="large-8 columns">
				<?php echo $aOutput['msg']; ?>

			</div>
		</div>
	</section>
	
	
</div><!--END TABLET AND DESKTOP UP ONLY-->

<?php
	include("footer.php");
?>