<?php
	include($_SERVER['DOCUMENT_ROOT']. '/tyfoon/connect.php');
	$aPage = pageGet(66);
	$aPage2 = pageGet(67);
	$aPage3 = pageGet(76);	
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'About';
	$cSEOTitle = '';
	$layout = 'home';
?>

<?php
	include("header.php");
?>

<div class="main-container">


	<section class="subpage-blue">
		<h1>About Us <span class="header-icon"><img src="img/about-icon-blue.svg" alt="about-us-icon" span="" /></span></h1>
		<?=$aPage3['msg']?>

		<div class="row">
			<div class="large-8 columns">
				<h2>Leadership</h2>
			</div>
		</div>
		<div class="row">
			<div class="large-6 columns">
				<img src="img/profile-img-1.png" alt="profile-img-1" width="192" height="192" />
				<h3><?=$aPage['title']; ?></h3>
				<?=$aPage['msg']; ?>
			</div>
			<div class="large-6 columns">
				<img src="img/profile-img-2.png" alt="profile-img-1" width="192" height="192" />
				<h3><?=$aPage2['title']; ?></h3>
				<?=$aPage2['msg']; ?>
			</div>
		</div>
	</section>


</div><!--END TABLET AND DESKTOP UP ONLY-->

<?php
	include("footer.php");
?>
