	<footer>
		<div class="row">
			<div class="medium-6 columns logo-footer medium-offset-2">
				<p><img src="img/footer-pack-logo.png" alt="footer-pack-logo" /> Copyright <?php echo date('Y');?> All Rights Reserved <br /><a href="terms-of-use.php">Terms of Use</a> | <a href="privacy-policy.php">Privacy Policy</a></p>
			</div>
			<div class="medium-3 columns zeekee-slug">
				<a href="http://zeekeeinteractive.com" target="_blank"><img src="img/zeekee-slug-white-2011.png" alt="zeekee-slug-white-2011" width="120" height="19" /></a>
			</div>
		</div>
	</footer>
	</section><!--ENDS MAIN CONTENT AREA OFFCANVAS-->

	<a class="exit-off-canvas"></a>
	
  </div>
  
</div>
	


<script src="bower_components/jquery/jquery.js"></script>
<script src="bower_components/foundation/js/foundation.min.js"></script>
<script src="js/app.js"></script>
<script src="js/jquery.navgoco.js"></script>

</body>
</html>