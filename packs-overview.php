<?php
	include($_SERVER['DOCUMENT_ROOT']. '/tyfoon/connect.php');
	$aPage = pageGet(74);

	$cMetaDesc = $aPage['description'];
	$cMetaKW = $aPage['keywords'];
	$cPageTitle = $aPage['title'];
	$cSEOTitle = $aPage['abstract'];
	$layout = 'home';
?>

<?php
	include("header.php");
?>

<div class="main-container">


	<section class="subpage-blue">
		<h1><?php echo $aPage['title']; ?> <span class="header-icon"><img src="img/packs-icon-blue.png" alt="packs-icon-blue"></span></h1>
		<div class="row">
			<div class="large-12 columns">
				<p>PACK Health LLC, builds tangible change packages that empower today's patients and providers.</p>
			</div>
		</div>
		<div class="row">
			<div class="large-12 columns">
				<p><img src="img/pack-icons.png"></p>
			</div>
		</div>
	</section>

	<section class="welcome hide-for-small">
		<div class="row">
			<div class="large-12 columns">
				<img src="img/open-pack.jpg" alt="open-pack" />
			</div>
		</div>
	</section>
	<section class="patient-refer">
		<!--<div class="row">
			<div class="medium-6 columns">
				<article>
					<img src="img/for-patients-icon.png" alt="for-patients-icon" width="49" height="47" />
					<h2>patients</h2>
					
					<a href="patients.php">
						<div class="circle">
							<h3>Get your<br> pack</h3>
						</div>
					</a>
				</article>
			</div>
			<div class="medium-6 columns">
				<article>
					<img src="img/for-referrers-icon.png" alt="for-referrers-icon" width="59" height="53" />
					<h2>prescribers</h2>
					
					<a href="prescribers.php">
						<div class="circle">
							<h3>Learn more about packs</h3>
						</div>
					</a>
				</article>
			</div>
		</div>-->
	</section>
	
	
</div><!--END TABLET AND DESKTOP UP ONLY-->

<?php
	include("footer.php");
?>