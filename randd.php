<?php	
	include($_SERVER['DOCUMENT_ROOT']. '/tyfoon/connect.php');
	$aPage = pageGet(77);
	$aFeatured = pageByCategory('RESEARCH', 'ANY', 'PUBL_ASC');
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'Research &amp; Development';
	$cSEOTitle = '';
	$layout = 'home';
	
	function truncate_to_word($s1){
		$s1 = strip_tags(substr($s1, 0 ,200));
		$s1 = substr($s1, 0, strrpos($s1, ' '));
		return $s1;
	}
?>

<?php
	include("header.php");
?>

<div class="main-container">


	<section class="subpage-blue">
		<h1>Research &amp; Development <span class="header-icon"><img src="img/news-icon-blue.png" alt="news-us-icon"</span></h1>
		<div class="row">
			<div class="large-8 columns">
				<?=$aPage['msg']?>
			</div>
		</div>
		<div class="row">
			<div class="large-8 columns">
				<?php foreach($aFeatured as $aFeature){ ?>
					<article class="r-and-d">
						<div class="row">
							<div class="medium-12 columns">
								<h2><?=$aFeature['title']; ?></h2>
								<?=truncate_to_word($aFeature['msg']); ?>...<br><br>
								<a href="<?=$aFeature['addl_external_url']; ?>" class="button" target="_blank">Read More</a>
								<hr/>
							</div>
						</div><!-- /.row -->
					</article>
					
				<?php } ?>
			</div>
		</div>
	</section>

</div><!--END TABLET AND DESKTOP UP ONLY-->

<?php
	include("footer.php");
?>