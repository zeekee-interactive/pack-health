<?php
	include($_SERVER['DOCUMENT_ROOT']. '/tyfoon/connect.php');
	$aPage = pageGet(75);
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'Patients';
	$cSEOTitle = '';
	$layout = 'home';
?>

<?php
	include("header.php");
?>

<div class="main-container">

	<section class="subpage-blue">
		<h1>Got a prescription? <span class="header-icon"><img src="img/packs-icon-blue.png" alt="packs-icon-blue" span="" /></span></h1>
		<div class="row">
			<div class="large-8 columns">
				<form id="form38" name="form38" class="wufoo topLabel page" accept-charset="UTF-8" autocomplete="off" enctype="multipart/form-data" method="post" novalidate
      action="https://zeekee2.wufoo.com/forms/q1t7x90t1t0vyhv/#public">


<h2>Pack Health Pack Activation</h2>
<ul>

<li id="foli1" class="notranslate      ">
<label class="desc" id="title1" for="Field1">
Your Name
<span id="req_1" class="req">*</span>
</label>
<div>
<input id="Field1" name="Field1" type="text" class="field text large" value="" maxlength="255" tabindex="1" onkeyup="" required />
</div>
</li>
<li id="foli122" class="notranslate      ">
<label class="desc" id="title122" for="Field122">
Pack Code
<span id="req_122" class="req">*</span>
</label>
<div>
<input id="Field122" name="Field122" type="text" class="field text medium" value="" maxlength="255" tabindex="2" onkeyup="" required />
</div>
</li>
<li id="foli6" class="complex notranslate      ">
<label class="desc" id="title6" for="Field6">
Address
<span id="req_6" class="req">*</span>
</label>
<div>
<span class="full addr1">
<input id="Field6" name="Field6" type="text" class="field text addr" value="" tabindex="3" required />
<label for="Field6">Street Address</label>
</span>
<span class="full addr2">
<input id="Field7" name="Field7" type="text" class="field text addr" value="" tabindex="4" />
<label for="Field7">Address Line 2</label>
</span>
<span class="left city">
<input id="Field8" name="Field8" type="text" class="field text addr" value="" tabindex="5" required />
<label for="Field8">City</label>
</span>
<span class="right state">
<input id="Field9" name="Field9" type="text" class="field text addr" value="" tabindex="6" required />
<label for="Field9">State / Province / Region</label>
</span>
<span class="left zip">
<input id="Field10" name="Field10" type="text" class="field text addr" value="" maxlength="15" tabindex="7" required />
<label for="Field10">Postal / Zip Code</label>
</span>
<span class="right country">
<select id="Field11" name="Field11" class="field select addr" tabindex="8" >
<option value="" ></option>
<option value="United States" selected="selected">United States</option>
<option value="United Kingdom" >United Kingdom</option>
<option value="Australia" >Australia</option>
<option value="Canada" >Canada</option>
<option value="France" >France</option>
<option value="New Zealand" >New Zealand</option>
<option value="India" >India</option>
<option value="Brazil" >Brazil</option>
<option value="----" >----</option>
<option value="Afghanistan" >Afghanistan</option>
<option value="Åland Islands" >Åland Islands</option>
<option value="Albania" >Albania</option>
<option value="Algeria" >Algeria</option>
<option value="American Samoa" >American Samoa</option>
<option value="Andorra" >Andorra</option>
<option value="Angola" >Angola</option>
<option value="Anguilla" >Anguilla</option>
<option value="Antarctica" >Antarctica</option>
<option value="Antigua and Barbuda" >Antigua and Barbuda</option>
<option value="Argentina" >Argentina</option>
<option value="Armenia" >Armenia</option>
<option value="Aruba" >Aruba</option>
<option value="Austria" >Austria</option>
<option value="Azerbaijan" >Azerbaijan</option>
<option value="Bahamas" >Bahamas</option>
<option value="Bahrain" >Bahrain</option>
<option value="Bangladesh" >Bangladesh</option>
<option value="Barbados" >Barbados</option>
<option value="Belarus" >Belarus</option>
<option value="Belgium" >Belgium</option>
<option value="Belize" >Belize</option>
<option value="Benin" >Benin</option>
<option value="Bermuda" >Bermuda</option>
<option value="Bhutan" >Bhutan</option>
<option value="Bolivia" >Bolivia</option>
<option value="Bosnia and Herzegovina" >Bosnia and Herzegovina</option>
<option value="Botswana" >Botswana</option>
<option value="British Indian Ocean Territory" >British Indian Ocean Territory</option>
<option value="Brunei Darussalam" >Brunei Darussalam</option>
<option value="Bulgaria" >Bulgaria</option>
<option value="Burkina Faso" >Burkina Faso</option>
<option value="Burundi" >Burundi</option>
<option value="Cambodia" >Cambodia</option>
<option value="Cameroon" >Cameroon</option>
<option value="Cape Verde" >Cape Verde</option>
<option value="Cayman Islands" >Cayman Islands</option>
<option value="Central African Republic" >Central African Republic</option>
<option value="Chad" >Chad</option>
<option value="Chile" >Chile</option>
<option value="China" >China</option>
<option value="Colombia" >Colombia</option>
<option value="Comoros" >Comoros</option>
<option value="Democratic Republic of the Congo" >Democratic Republic of the Congo</option>
<option value="Republic of the Congo" >Republic of the Congo</option>
<option value="Cook Islands" >Cook Islands</option>
<option value="Costa Rica" >Costa Rica</option>
<option value="C&ocirc;te d'Ivoire" >C&ocirc;te d'Ivoire</option>
<option value="Croatia" >Croatia</option>
<option value="Cuba" >Cuba</option>
<option value="Cyprus" >Cyprus</option>
<option value="Czech Republic" >Czech Republic</option>
<option value="Denmark" >Denmark</option>
<option value="Djibouti" >Djibouti</option>
<option value="Dominica" >Dominica</option>
<option value="Dominican Republic" >Dominican Republic</option>
<option value="East Timor" >East Timor</option>
<option value="Ecuador" >Ecuador</option>
<option value="Egypt" >Egypt</option>
<option value="El Salvador" >El Salvador</option>
<option value="Equatorial Guinea" >Equatorial Guinea</option>
<option value="Eritrea" >Eritrea</option>
<option value="Estonia" >Estonia</option>
<option value="Ethiopia" >Ethiopia</option>
<option value="Faroe Islands" >Faroe Islands</option>
<option value="Fiji" >Fiji</option>
<option value="Finland" >Finland</option>
<option value="Gabon" >Gabon</option>
<option value="Gambia" >Gambia</option>
<option value="Georgia" >Georgia</option>
<option value="Germany" >Germany</option>
<option value="Ghana" >Ghana</option>
<option value="Gibraltar" >Gibraltar</option>
<option value="Greece" >Greece</option>
<option value="Grenada" >Grenada</option>
<option value="Guatemala" >Guatemala</option>
<option value="Guinea" >Guinea</option>
<option value="Guinea-Bissau" >Guinea-Bissau</option>
<option value="Guyana" >Guyana</option>
<option value="Haiti" >Haiti</option>
<option value="Honduras" >Honduras</option>
<option value="Hong Kong" >Hong Kong</option>
<option value="Hungary" >Hungary</option>
<option value="Iceland" >Iceland</option>
<option value="Indonesia" >Indonesia</option>
<option value="Iran" >Iran</option>
<option value="Iraq" >Iraq</option>
<option value="Ireland" >Ireland</option>
<option value="Israel" >Israel</option>
<option value="Italy" >Italy</option>
<option value="Jamaica" >Jamaica</option>
<option value="Japan" >Japan</option>
<option value="Jordan" >Jordan</option>
<option value="Kazakhstan" >Kazakhstan</option>
<option value="Kenya" >Kenya</option>
<option value="Kiribati" >Kiribati</option>
<option value="North Korea" >North Korea</option>
<option value="South Korea" >South Korea</option>
<option value="Kuwait" >Kuwait</option>
<option value="Kyrgyzstan" >Kyrgyzstan</option>
<option value="Laos" >Laos</option>
<option value="Latvia" >Latvia</option>
<option value="Lebanon" >Lebanon</option>
<option value="Lesotho" >Lesotho</option>
<option value="Liberia" >Liberia</option>
<option value="Libya" >Libya</option>
<option value="Liechtenstein" >Liechtenstein</option>
<option value="Lithuania" >Lithuania</option>
<option value="Luxembourg" >Luxembourg</option>
<option value="Macedonia" >Macedonia</option>
<option value="Madagascar" >Madagascar</option>
<option value="Malawi" >Malawi</option>
<option value="Malaysia" >Malaysia</option>
<option value="Maldives" >Maldives</option>
<option value="Mali" >Mali</option>
<option value="Malta" >Malta</option>
<option value="Marshall Islands" >Marshall Islands</option>
<option value="Mauritania" >Mauritania</option>
<option value="Mauritius" >Mauritius</option>
<option value="Mexico" >Mexico</option>
<option value="Micronesia" >Micronesia</option>
<option value="Moldova" >Moldova</option>
<option value="Monaco" >Monaco</option>
<option value="Mongolia" >Mongolia</option>
<option value="Montenegro" >Montenegro</option>
<option value="Morocco" >Morocco</option>
<option value="Mozambique" >Mozambique</option>
<option value="Myanmar" >Myanmar</option>
<option value="Namibia" >Namibia</option>
<option value="Nauru" >Nauru</option>
<option value="Nepal" >Nepal</option>
<option value="Netherlands" >Netherlands</option>
<option value="Netherlands Antilles" >Netherlands Antilles</option>
<option value="Nicaragua" >Nicaragua</option>
<option value="Niger" >Niger</option>
<option value="Nigeria" >Nigeria</option>
<option value="Norway" >Norway</option>
<option value="Oman" >Oman</option>
<option value="Pakistan" >Pakistan</option>
<option value="Palau" >Palau</option>
<option value="Palestine" >Palestine</option>
<option value="Panama" >Panama</option>
<option value="Papua New Guinea" >Papua New Guinea</option>
<option value="Paraguay" >Paraguay</option>
<option value="Peru" >Peru</option>
<option value="Philippines" >Philippines</option>
<option value="Poland" >Poland</option>
<option value="Portugal" >Portugal</option>
<option value="Puerto Rico" >Puerto Rico</option>
<option value="Qatar" >Qatar</option>
<option value="Romania" >Romania</option>
<option value="Russia" >Russia</option>
<option value="Rwanda" >Rwanda</option>
<option value="Saint Kitts and Nevis" >Saint Kitts and Nevis</option>
<option value="Saint Lucia" >Saint Lucia</option>
<option value="Saint Vincent and the Grenadines" >Saint Vincent and the Grenadines</option>
<option value="Samoa" >Samoa</option>
<option value="San Marino" >San Marino</option>
<option value="Sao Tome and Principe" >Sao Tome and Principe</option>
<option value="Saudi Arabia" >Saudi Arabia</option>
<option value="Senegal" >Senegal</option>
<option value="Serbia" >Serbia</option>
<option value="Seychelles" >Seychelles</option>
<option value="Sierra Leone" >Sierra Leone</option>
<option value="Singapore" >Singapore</option>
<option value="Slovakia" >Slovakia</option>
<option value="Slovenia" >Slovenia</option>
<option value="Solomon Islands" >Solomon Islands</option>
<option value="Somalia" >Somalia</option>
<option value="South Africa" >South Africa</option>
<option value="Spain" >Spain</option>
<option value="Sri Lanka" >Sri Lanka</option>
<option value="Sudan" >Sudan</option>
<option value="Suriname" >Suriname</option>
<option value="Swaziland" >Swaziland</option>
<option value="Sweden" >Sweden</option>
<option value="Switzerland" >Switzerland</option>
<option value="Syria" >Syria</option>
<option value="Taiwan" >Taiwan</option>
<option value="Tajikistan" >Tajikistan</option>
<option value="Tanzania" >Tanzania</option>
<option value="Thailand" >Thailand</option>
<option value="Togo" >Togo</option>
<option value="Tonga" >Tonga</option>
<option value="Trinidad and Tobago" >Trinidad and Tobago</option>
<option value="Tunisia" >Tunisia</option>
<option value="Turkey" >Turkey</option>
<option value="Turkmenistan" >Turkmenistan</option>
<option value="Tuvalu" >Tuvalu</option>
<option value="Uganda" >Uganda</option>
<option value="Ukraine" >Ukraine</option>
<option value="United Arab Emirates" >United Arab Emirates</option>
<option value="United States Minor Outlying Islands" >United States Minor Outlying Islands</option>
<option value="Uruguay" >Uruguay</option>
<option value="Uzbekistan" >Uzbekistan</option>
<option value="Vanuatu" >Vanuatu</option>
<option value="Vatican City" >Vatican City</option>
<option value="Venezuela" >Venezuela</option>
<option value="Vietnam" >Vietnam</option>
<option value="Virgin Islands, British" >Virgin Islands, British</option>
<option value="Virgin Islands, U.S." >Virgin Islands, U.S.</option>
<option value="Yemen" >Yemen</option>
<option value="Zambia" >Zambia</option>
<option value="Zimbabwe" >Zimbabwe</option>
</select>
<label for="Field11">Country</label>
</span>
</div>
</li>
<li id="foli12" class="phone notranslate      ">
<label class="desc" id="title12" for="Field12">
Cell Phone
<span id="req_12" class="req">*</span>
</label>
<span>
<input id="Field12" name="Field12" type="tel" class="field text" value="" size="3" maxlength="3" tabindex="9" required />
<label for="Field12">###</label>
</span>
<span class="symbol">-</span>
<span>
<input id="Field12-1" name="Field12-1" type="tel" class="field text" value="" size="3" maxlength="3" tabindex="10" required />
<label for="Field12-1">###</label>
</span>
<span class="symbol">-</span>
<span>
 <input id="Field12-2" name="Field12-2" type="tel" class="field text" value="" size="4" maxlength="4" tabindex="11" required />
<label for="Field12-2">####</label>
</span>
</li>
<li id="foli13" class="phone notranslate      ">
<label class="desc" id="title13" for="Field13">
Home/Work Phone
</label>
<span>
<input id="Field13" name="Field13" type="tel" class="field text" value="" size="3" maxlength="3" tabindex="12" />
<label for="Field13">###</label>
</span>
<span class="symbol">-</span>
<span>
<input id="Field13-1" name="Field13-1" type="tel" class="field text" value="" size="3" maxlength="3" tabindex="13" />
<label for="Field13-1">###</label>
</span>
<span class="symbol">-</span>
<span>
 <input id="Field13-2" name="Field13-2" type="tel" class="field text" value="" size="4" maxlength="4" tabindex="14" />
<label for="Field13-2">####</label>
</span>
</li>
<li id="foli14" class="notranslate      ">
<label class="desc" id="title14" for="Field14">
Email
</label>
<div>
<input id="Field14" name="Field14" type="email" spellcheck="false" class="field text large" value="" maxlength="255" tabindex="15" />
</div>
</li>
<li id="foli17" class="notranslate      ">
<fieldset>
<![if !IE | (gte IE 8)]>
<legend id="title17" class="desc">
<span id="req_17" class="req">*</span>
</legend>
<![endif]>
<!--[if lt IE 8]>
<label id="title17" class="desc">
<span id="req_17" class="req">*</span>
</label>
<![endif]-->
<div>
<span>
<input id="Field17" name="Field17" type="checkbox" class="field checkbox" value="Do you accept the Terms of Use and Privacy Policy of Pack Health?" tabindex="16" />
<label class="choice" for="Field17">Do you accept the Terms of Use and Privacy Policy of Pack Health?</label>
</span>
</div>
</fieldset>
</li> <li class="buttons ">
<div>

<input id="saveForm" name="saveForm" class="button" type="submit" value="Submit" /></div>
</li>

<li class="hide">
<label for="comment">Do Not Fill This Out</label>
<textarea name="comment" id="comment" rows="1" cols="1"></textarea>
<input type="hidden" id="idstamp" name="idstamp" value="uI7nVs0U+hRL07udE+FoX5qaXXPwEmWN64g0Z9ZpXmo=" />
</li>
</ul>
</form>
			</div><!-- /.columns -->
			<div class="large-4 columns more-instructions">
				<ul class="no-bullet">
					<li><span>1</span> Please enter your details in the form</li>
					<li><span>2</span> We will send your starter Pack to you</li>
					<li><span>3</span> We will engage with you for 12 weeks</li>
				</ul>
			</div>
		</div><!-- /.row -->
	</section>

</div><!--END TABLET AND DESKTOP UP ONLY-->

<?php
	include("footer.php");
?>
