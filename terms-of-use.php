<?php
	include($_SERVER['DOCUMENT_ROOT']. '/tyfoon/connect.php');
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'Terms of Use';
	$cSEOTitle = '';
	$layout = 'home';
?>

<?php
	include("header.php");
?>

<div class="main-container">


	<section class="subpage-blue">
		<h1>Terms and Conditions <span class="header-icon"><img src="img/packs-icon-blue.png" alt="packs-us-icon"</span></h1>
		<div class="row">
			<div class="large-8 columns">
				<h2>PACK Health LLC Website Terms and Conditions of Use</h2>
				
				<p>The www.packhealth.com website (the "Website") is provided by PACK Health, LLC (sometimes referred to herein as "PACKHealth", "we", "us" or "our") for your convenience and for general information purposes only in conjunction with use of the health information kit prescribed by your physician (your "PACK").  All access to and use of the Website, including any use or reliance on the information on the Website, is provided subject to these Website Terms and Conditions of Use (the "Terms").  By accessing the Website or downloading or using the information on the Website, you agree to be bound by these Terms. <br />
					
					NO MEDICAL ADVICE / MEDICAL EMERGENCIES<br /><br />
Although medical and health information may be provided on the Website, the material is for informational purposes only and is not a substitute for proper professional medical advice, diagnosis, treatment or care.  Always seek the advice of a physician or other qualified health provider with any questions you may have regarding a medical condition or treatment option.  In no event shall PACKHealth be liable to you or to anyone else for any decision or action taken by you or your caregiver in reliance on the information available on the Website.  
IF YOU THINK YOU MAY HAVE A MEDICAL EMERGENCY, CALL YOUR DOCTOR OR 911 IMMEDIATELY. <br /><br />
CONTENT<br /><br />
The information contained in this website may include educational and informational material to augment or further the materials contained in your Pack, and may include, as examples only, care plans, training programs, risk management tools, life style modification information and disease management tools (all information available on the Website is collectively referred to in these Terms as the "Content").   The Content is provided for your personal use only.  You may not sell, give, or otherwise distribute the Content to anyone, other than by referring others to this Website.  PACKHealth allows you to access, view, use, display and download the Website and the Content, but you do not receive nay ownership in or to the Website or the content or any copies thereof.  Your rights are limited to use of the content for your personal benefit and you may share the Content only with your physician(s) and your caregiver.  Organizations, companies and/or businesses may not become members of the Website and may not use the website or Content without the prior express written consent of PACKHealth.  Neither the Website, the Content, nor any portion thereof may be modified, copied, reproduced, downloaded, posted, transmitted, transferred, sold or distributed in any form.  <br />
The Website contains material which is protected by copyright and trademark laws and is exclusively owned by PACKHealth and/or its licensors, including, but not limited to, the PACKHealth and PACK names, the Content and the Website layout.  <br />
Your rights to access, view, use, display and download the are subject to the following restrictions: 1) you must retain, on all copies of the Content, all copyright and other proprietary notices contained in the Content on the Website; 2) you may not modify the Content in any way or reproduce or publicly display, perform, or distribute or otherwise use them for any public or commercial purpose; and 3) you must not transfer the Content to any other person. You agree to abide by all additional restrictions displayed on the Website as it may be updated from time to time. You agree to comply with all copyright and other laws worldwide in your use of this Website and to prevent any unauthorized copying of the Content. Except as expressly provided, PACKHealth does not grant any express or implied right to you under any patents, trademarks, copyrights or trade secret information.<br /><br />
LIMITATIONS ON USE OF THE WEBSITE <br /><br />
You agree to use this Website only for lawful purposes. <br />
We may prohibit or enjoin any conduct or use of this Website or the Content that restricts or prevents other users from using the Website or Content or that otherwise violates these Terms.  We have the right to restrict, suspend or terminate access to any user for any reason at any time in our sole discretion. Notwithstanding the above, we have no obligation to monitor the use of the Website, but reserve the right to do so.  PACKHealth has no obligation to maintain, store, or transfer to you information or data that you have posted on or uploaded to the Website.<br />
You shall not post or upload any information or other materials to the Website that (a) are false, inaccurate or misleading; (b) are obscene or indecent; (c) infringe any copyright, patent, trademark, trade secret or other proprietary rights or rights of publicity or privacy of any party; or (d) are defamatory, libelous, threatening, abusive, hateful, or contains pornography. Members shall not interfere with other members’ use and enjoyment of the Website. <br />
You may not use any robot, spider, scraper, or other automated means to access the Website or the Content for any purposes. You may not post any material on the Website that contains any viruses, Trojan horses, worms, time bombs, spiders, or other computer programming routines that are intended to damage, detrimentally interfere with, surreptitiously intercept or expropriate any system, data or personal information. You shall not attempt to make the Website and/or the Content unavailable through denial-of-service attacks or similar means. You shall not use contact information provided by users, or collect information about our users, to facilitate the sending of unsolicited bulk communications such as SPAM.<br />
If you submit to the Website or otherwise exchange communications, content or materials via the Website, including, any personal or commercial information, ideas, concepts or inventions, you irrevocably grant to PACKHealth an unrestricted, worldwide, royalty free license to use, reproduce, display publicly, perform, publish, transmit and distribute such materials and you further agree that PACKHealth is free to use any ideas, concepts or know-how that you provide to PACKHealth.  <br /><br />
PRIVACY<br /><br />
Privacy and information security are important to PACKHealth.  Use of the Website and the Content is governed by PACKHealth’s Privacy Policy which is accessible [here].  By accepting these Terms, you agree to be bound by PACKHealth's Privacy Policy as well as the Terms. <br /><br />
LINKS<br /><br />
Through the Website you are able to link to other websites which are not under the control of PACKHealth. We have no control over the nature, content and availability of those sites. The inclusion of any links on the Website is not and does not imply a recommendation or endorsement of the views expressed with the linked websites.  All use of the linked websites is subject to the terms and conditions of the linked websites.<br /><br />
WARRANTIES<br /><br />
All users represent and warrant that the information they provided when accessing the Website and/or registering as a member, and all information that they subsequently provide regarding themselves and their membership, is true and accurate and not misleading.<br />
Although PACKHealth attempts to keep the Content accurate, up-to-date, and complete PACKHealth cannot attest at all times to, and makes no warranty regarding the quality, accuracy, completeness, timeliness, or correctness of the Content. PACKHealth makes no warranties, express or implied, regarding errors or omissions and assumes no liability for loss or damage resulting from the use of Content and/or the Website. ALL INFORMATION ON THIS WEBSITE IS PROVIDED "AS IS", "WITH ALL FAULTS", AND "AS AVAILABLE." <br />
PACKHealth makes no warranty and expressly disclaims all warranties that the Website or the Content will be uninterrupted or error free, free of transmission errors, or free of viruses or other harmful components or programs.  PACKHealth FURTHER DISCLAIMS ALL WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULARLY PURPOSE AND NON-INFRINGEMENT.<br />
When you use the Website, any information that you submit will be transmitted via the Internet and such transmission is beyond our control.  We do not assume any liability for or relating to the delay, failure, interruption, or corruption of any data or other information transmitted in connection with use of this Website.  We do not assume any liability for loss or damage to your computer systems or networks as a result of your use of this Website.  While we use reasonable security measures to protect use of the Website and the Content, electronic storage of and access to data is never 100% secure or guaranteed.<br />
The advice, recommendations, information, and conclusions posted or emailed by other users of the website are not in any way vetted, approved or endorsed by PACKHealth, and you use such information at your own risk.<br /><br />
LIMITATION OF LIABILITY<br /><br />
UNDER NO CIRCUMSTANCES SHALL PACKHealth  or its SUBSIDIARIES, AFFILIATES, OFFICERS, DIRECTORS, EMPLOYEES, AGENTS OR REPRESENTATIVES BE LIABLE FOR DIRECT, INDIRECT, INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES, INCLUDING, WITHOUT LIMITATION, LOSSES, COSTS OR DAMAGES SUFFERED OR INCURRED AS A RESULT OF USE OF THE WEBSITE, THE CONTENT, OR ANY OTHER INFORMATION OR MATERIAL ON THE WEBSITE OR SUFFERED OR INCURRED AS A RESULT OF FAULTS, INTERRUPTIONS, OR DELAYS IN THE USE OF THE WEBSITE, OR OUT OF ANY INACCURACIES, ERRORS, OR OMISSIONS IN THE CONTENT, OR ANY OTHER INFORMATION OR MATERIALS ON THE WEBSITE, REGARDLESS OF HOW SUCH FAULTS, INTERRUPTIONS, DELAYS, INACCURACIES, ERRORS, OR OMISSIONS ARISE, EVEN IF WE HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.  WITHOUT LIMITING THE FOREGOING, IN NO EVENT WILL PACKHealth BE LIABLE FOR ANY LOSSES OR DAMAGES, IN THE AGGREGATE, IN EXCESS OF THE LESSER OF THE AMOUNT YOU HAVE PAID TO PACKHealth, IF ANY, FOR ACCESS TO THE WEBSITE IN THE TWELVE MONTHS IMMEDIATELY PRECEDING THE EVENT GIVING RISE TO THE LOSS OR DAMAGE OR TWENTY-FIVE DOLLARS (US $25.00), NOTWITHSTANDING ANY CLAIM THAT SUCH REMEDY FAILS OF ITS ESSENTIAL PURPOSE.  If you are dissatisfied with any content, or any other materials or information provided on this Website or with the use of this Website, your sole and exclusive remedy is to discontinue using the Website.  <br /><br />
INDEMNIFICATION<br /><br />
If you cause a technical disruption of the Website, you are responsible for any and all liabilities, costs and expenses (including reasonable attorneys' fees and costs of enforcement) arising from or related to that disruption.  You agree to indemnify, defend, and hold harmless PACKHealth and its officers, employees, agents, subsidiaries, affiliates, directors and partners, from and against any third party action, suit, claim or demand and any associated losses, expenses, damages, costs and other liabilities (including reasonable attorneys' fees), arising out of or relating to your use or misuse of any portion of the Website and/or the Content, or your violation of these Terms.<br /><br />
GOVERNING LAW / VENUE<br /><br />
This Agreement shall be governed by the laws of the state of Alabama and the applicable federal laws of the United States. You agree that all disputes arising under, or in any way connected with the Website, the Content, or the use thereof shall be litigated exclusively in the state and federal courts residing in the state of Alabama, county of Jefferson, and in no other court or jurisdiction.<br /><br />
INTERNATIONAL USERS<br /><br />
The Website is created, operated and controlled by PACKHealth  from within the United States. PACKHealth makes no claims that Website or the Content are appropriate for use or downloading outside of the United States. You may not use the Website or the Content in violation of United States export laws and regulations.  If you are a non-United States user, you acknowledge and agree that, as a result of your use of the Website, PACKHealth may collect and use your information as disclosed in these Terms and our Privacy Policy and that such information may be stored on servers located outside your resident jurisdiction. United States law may not provide the same degree of protection for such information that is available in your home country. By providing us with your information, you acknowledge that you consent to the transfer of such information outside your resident jurisdiction as detailed in our Privacy Policy. If you do not consent to such transfer, you may not use the Website or the Content.  <br /><br />
CHANGES TO THE WEBSITE AND/OR THE TERMS<br /><br />
The Website and Content are subject to change without prior notice. PACKHealth may occasionally update these Terms.  Notices of changes will be posted on the Website. Your use of the Website or Content following a change in the Terms indicates your consent to the revised Terms.  You agree that we shall not be liable to you or any third-party for any modification to or discontinuance of any Content, features, services or information generally available on the Website or the Website itself.<br /><br />
COPYRIGHT SAFE HARBOR<br /><br />
We have adopted the following policy toward copyright and intellectual property infringement.  The address of our Designated Agent to Receive Notification of Claimed Infringement ("Designated Agent") is:
Privacy Officer<br />
PACK Health, LLC<br />
1500 1st Avenue North<br />
#52<br />
Birmingham, AL 35203<br />
info@packhealth.com<br /><br />

If you believe that material residing on or accessible through this Website infringes your copyright or other intellectual property right, provide PACKHealth notice of such infringement, by sending a written notice of the infringement to the Designated Agent listed above. Please specify the type of infringement at issue and the notice must include the following information:<br />
•	A signature (physical or electronic) of a person authorized to act on behalf of the owner of the copyright or intellectual property interest;<br />
•	A description of the work that you claim has been infringed upon; <br />
•	A description of where the material you claim is infringing is located on the Website (in sufficient detail to allow us to locate it on the Website); <br />
•	Your address, telephone number and email address; <br />
•	A statement by you that you have a good-faith belief that the disputed use is not authorized by the owner of the work, its agent or the law; and <br />
•	A statement by you, under penalty of perjury that the information you submit is accurate and that you are the owner of the intellectual property or authorized to act on behalf of the owner. 
Once a proper notice is received by our Designated Agent, PACKHealth will investigate the allegations in the notice, and if appropriate, remove or disable access to the allegedly infringing material. 
<br /><br />
MISCELLANEOUS <br /><br />
PACKHEALTH and PACK are trademarks of PACKHealth, LLC. You agree not to display or use these trademarks in any manner without a prior, written permission from PACKHealth.<br />
PACKHealth may assign the Website, the Content and its rights under these Terms at any time to a subsidiary or parent company or to a successor to its business as part of a merger or sale of substantially all of its assets. You may not assign or transfer your rights under these Terms.<br />
If any provision of these Terms is held to be unenforceable for any reason, the remaining   provisions will be unaffected and remain in full force and effect.  No waiver of any of these Terms shall be deemed a further or continuing waiver of such term or condition.<br />
If for any reason a court of competent jurisdiction finds any provision of the Terms or any portion thereof, to be unenforceable, that provision shall be enforced to the maximum extent permissible so as to affect the intent of the Terms, and the remainder of these Terms shall continue in full force and effect.
For any questions or comments, or to report violations of these Terms contact us at:<br /><br />
PACK Health, LLC<br />
1500 1st Avenue North<br />
#52<br />
Birmingham, AL 35203<br />
info@packhealth.com<br />

				</p>
				
				
			</div>
		</div>
	</section>
	
	
</div><!--END TABLET AND DESKTOP UP ONLY-->

<?php
	include("footer.php");
?>