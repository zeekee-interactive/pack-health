<?php
	include 'postman/_variables.php';
	include($_SERVER['DOCUMENT_ROOT']. '/tyfoon/connect.php');
	$aPage = pageGet(78);
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'Contact Us';
	$cSEOTitle = '';
	$layout = 'home';
?>

<?php
	include("header.php");
	
?>

<div class="main-container">

	<section class="subpage-blue">
		<h1>Contact Us <span class="header-icon"><img src="img/contact-icon-blue.png" alt="contact-icon-blue"</span></h1>
		<div class="row">
			<div class="large-8 columns">
				<p>For more information or inquiries, please contact us.</p>				
			</div>
		</div>
		<hr/>
		<div class="row">
			<div class="large-4 columns">
				<?php echo !empty( $cError ) ? '<div class="error">' . $cError . '</div>' : false;

					if ( $_SERVER['REQUEST_METHOD'] == 'GET' || !empty( $_POST ) ) {
				?>
				<form action="<?=$_SERVER['PHP_SELF'] ?>" method="POST" id="foonster" name="foonster" enctype="multipart/form-data">
					<div class="row">
						<div class="medium-12 columns">
							<label>Your Name</label>
							<input type="text" value="<?=$_POST['name'] ?>" id="name" name="name" />
						</div>
					</div>
					<div class="row">
						<div class="medium-12 columns">
							<label>Your Email</label>
							<input type="text" value="<?=$_POST['email'] ?>" id="email" name="email" />
						</div>
					</div>
					<div class="row">
						<div class="medium-12 columns">
							<label>Your Phone</label>
							<input type="text" value="<?=$_POST['phone'] ?>" id="phone" name="phone" />
						</div>
					</div>
			</div>
			
			<div class="large-4 columns">
					<div class="row">
						<div class="medium-12 columns message-push">
							<label>Your Message</label>
							<textarea value="<?=$_POST['msg']?>" name="msg"></textarea>
						</div>
					</div>
					<div class="row">
						<div class="medium-12 columns">
							<input type="submit" class="button" name="sbmtbtn" id="sbmtbtn" value="Submit">
						</div>
					</div>
				</form>
				<?php } ?>
			</div>
			
			<div class="large-4 columns contact-info">
				<?=$aPage['msg']?>
			</div>
		</div>
	</section>
	
</div><!--END TABLET AND DESKTOP UP ONLY-->

<?php
	include("footer.php");
?>