<!doctype html>
<html class="no-js" lang="en">
  <!-- <![endif] -->
  <head>
    <meta charset='utf-8'>
    <meta name="viewport" content="width=device-width" />
    <meta content='<?php echo $cMetaDesc; ?>' name='description'>
    <meta content='<?php echo $cMetaKW; ?>' name='keywords'>
    <meta name="google-site-verification" content="aIDpT_xeyWwUtxADrXAwIUlFiEOIp-DcCLn0puxEo3o" />
    <title><?php echo $cPageTitle; ?> - Pack Health</title>
    <link rel="stylesheet" href="stylesheets/app.css" />
<!-- WUFOO FORM STYLESHEETS -->
    <link rel="stylesheet" href="stylesheets/wufoo/form.css" />
    <link rel="stylesheet" href="stylesheets/wufoo/structure.css" />
    <link rel="stylesheet" href="stylesheets/wufoo/theme.css" />
<!-- WUFOO FORM STYLESHEETS -->
    <link rel="stylesheet" href="//brick.a.ssl.fastly.net/Lato:400,400i,700,700i">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<link rel="icon" href="favicon.ico" type="image/x-icon">
    <script src="js/wufoo.js"></script>
    <script src="bower_components/modernizr/modernizr.js"></script>
    	<!-- IE Fix for HTML5 Tags -->
    	<!--[if lt IE 9]>
      	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    	<![endif]-->
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-50909417-1', 'zeekeeinteractive.com');
		  ga('send', 'pageview');

		</script>
  </head>
  <body class="<?=$layout ?>">
<header>

</header>
<div class="off-canvas-wrap" data-offcanvas>
  <div class="inner-wrap">
    <nav class="tab-bar hide-for-medium-up">
      <section class="left-small">
        <a class="left-off-canvas-toggle menu-icon" href="#"><span></span><img src="img/pack-logo.png" alt="pack-logo" /></a>
      </section>
    </nav>

    <aside class="left-off-canvas-menu show-for-small">
      <ul class="off-canvas-list">
        <li><label>Pack Health</label></li>
        <li><a href="index.php"><img src="img/home-img.png" alt="home-img" />
			Home
		</a></li>
        <li><a href="patients.php"><img src="img/packs-icon.png" alt="packs-icon" />
			Packs</a>
        	<!--<ul>
	        	<li><a href="packs-overview.php">Overview</a></li>
	        	<li><a href="patients.php">Patients</a></li>
	        	<li><a href="prescribers.php">Prescribers</a></li>
	        	<li><a href="chronic-diseases.php">Chronic Diseases</a></li>
        	</ul>-->
        </li>
        <li><a href="about.php"><img src="img/about-icon.png" alt="about-icon" />
        	About Us
        </a></li>
        <li><a href="randd.php"><img src="img/news-icon.png" alt="news-icon" />
        	R &amp; D
        </a></li>
        <li><a href="contact.php"><img src="img/contact-icon.png" alt="contact-icon" />
        	Contact Us
        </a></li>
      </ul>
    </aside>

<section class="main-section"><!--MAIN CONTENT AREA START-->
	<!--BEGIN MAIN SIDEBAR NAVIGATION -->
		<section class="main-nav hide-for-small">
			<div class="logo-wrap">
				<a href="index.php"><img src="img/pack-logo.png" alt="pack-logo" /></a>
			</div>
			<ul>
				<li>
					<a href="index.php">
						<img src="img/home-img.png" alt="home-img" />
						<span>HOME</span>
					</a>
				</li>
				<li>
					<a href="patients.php">
						<img src="img/packs-icon.png" alt="packs-icon" />
						<!--<span class="packs-dropdown">packs</span>-->
						<span>packs</span>
					</a>
					<!--<ul class="dropdown">
						<li><a href="packs-overview.php">Overview<br><hr class="dd-divider"/></a></li>
						<li><a href="patients.php">patients<br><hr class="dd-divider"/></a></li>
						<li><a href="prescribers.php">prescribers<br><hr class="dd-divider"/></a></li>
	        			<li><a href="chronic-diseases.php">Chronic Diseases</a></li>
					</ul>-->
				</li>
				<li>
					<a href="about.php">
						<img src="img/about-icon.png" alt="about-icon" />
						<span>about us</span>
					</a>
				</li>
				<li>
					<a href="randd.php">
						<img src="img/news-icon.png" alt="news-icon" />
						<span>r &amp; d</span>
					</a>
				</li>
				<li>
					<a href="contact.php">
						<img src="img/contact-icon.png" alt="contact-icon" />
						<span>contact us</span>
					</a>
				</li>
			</ul>
		</section>
		<!--END MAIN SIDEBAR NAVIGATION -->
