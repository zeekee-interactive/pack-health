<?php
	include($_SERVER['DOCUMENT_ROOT']. '/tyfoon/connect.php');
	$cMetaDesc = '';
	$cMetaKW = '';
	$cPageTitle = 'Terms of Use';
	$cSEOTitle = '';
	$layout = 'home';
?>

<?php
	include("header.php");
?>

<div class="main-container">


	<section class="subpage-blue">
		<h1>Privacy Policy <span class="header-icon"><img src="img/packs-icon-blue.png" alt="packs-us-icon"</span></h1>
		<div class="row">
			<div class="large-8 columns">
				<h2>PACK Health LLC Privacy Policy</h2>
				
				<p>Welcome to PACK Health!  PACK Health (or "we," "us," or "our") is glad that you are following your doctor's advice and have taken this step to improve your health.  Our PACK Kits and the assistance we provide in conjunction with the PACK Kits will provide information and tools that you can use in conjunction with the program prescribed by your doctor to learn more about your condition, how to take care of yourself and how to monitor your success (our "Services").  This website, our mobile app and our Services are intended for use only as a supplement to, and not in place of, the care recommended by your doctor. <br /><br />

This Privacy Policy explains the information that we collect about you and your health and the ways that we use that information and protect its privacy.  This Privacy Policy applies only to the Services provided by PACK Health.<br /><br />

By submitting your information to us and/or using our Services, you consent to the terms of this Privacy Policy.  If you do not agree to the terms of this Privacy Policy and/or the treatment of your information as outlined in this policy, do not use our Services.  <br /><br />

Personal Information<br /><br />
We us the term "Personally Identifiable Information" in this Privacy Policy to mean any information about you that would allow a third party to determine your identity.  Information that does not personally identify you is referred to in this Privacy Policy as "Non personally Identifiable Information."<br /><br />

Collection And Use Of Information<br /><br />
Information that You Provide to Us: <br />
To order/activate your PACK Kit and allow us to provide you with the Services you have requested, you provide us with your name, mailing address, email address, phone number, and prescribing physician's name.  For some PACK Kits, in order to provide you with the best recommendations, we will ask for your birth date and gender.  If you respond to a survey, test a particular product or service for us, or enter a contest or promotional event you may need to provide additional types of demographic information. <br /><br />

Non-Personally Identifiable Information that We Collect:<br /><br />
PACK Health automatically collects certain technical information, such as the Internet address from which you visit our website, your browser type, which websites you were visiting before coming to our Website, which pages of our Website you visit and how long you visit each page.  In addition, if you use our mobile application, we will collect information about the mobile device you are using.  <br /><br />

Use of the Information:<br /><br />
We use your Personally Identifiable Information to provide the Services you have requested.  Those Services may include sending texts, emails, or regular mail or making phone calls to you to provide reminders, educational or technical information, or respond to your questions.  We may also use these methods to send you technical updates or notices regarding our Services or to improve our customer service.  We may also use your information to investigate, prevent or take action regarding illegal activities.<br /><br />

PACK Health may use your Personally Identifiable Information to inform you of other products or services available from PACK Health or our affiliates.  PACK Health may also contact you via surveys to conduct research about your opinion of current Services or of potential new Services that may be offered.<br /><br />

PACK Health compiles your Non-personally Identifiable Information in aggregate form to help diagnose problems with our servers and systems and to gather broad demographic information. PACK Health may provide aggregated, non-personally identifiable information on results from use of the PACK Kits or general use and adoption of the PACK Kits to customers and potential customers.  PACK Health may use Internet addresses for identification purposes only when necessary to enforce compliance with the PACK Health Terms of Use and to protect our Services or any person, as otherwise permitted by law.<br /><br />  

Sharing Of Your Information<br /><br />
Except as specified in this Privacy Policy, PACK Health will not sell, trade, lend or otherwise voluntarily disclose any Personally Identifiable Information that you have provided.  PACK Health may share your Personal Identifiable Information with third parties to assist in the administration of our website or the Services; to PACK Health financial and legal advisors; to others as required by law; or for purposes identified from time to time and consented by you. Where you have given us specific permission to do so, we may share your information with the doctor who prescribed your PACK Kit or with your insurance carrier.  Where personal information is shared with service providers for administrative purposes, we contractually require the service provider to respect this Privacy Policy.  Notwithstanding any other provision in this Privacy Policy, in the event someone buys or acquires PACK Health, or substantially all of its assets, information about you and your use of the Services will likely be one of the assets acquired and information may be disclosed to a potential purchaser or the purchaser.<br /><br />

We may share with third parties aggregated or deidentified data such as statistics about our customers, their use of the PACK Kits or results achieved through use of the PACK Kits.  If you post a comment or other information on a blog or other comment area of our website, you should remember that it will be publicly available.  <br /><br />

Your Ability To Update Your Personally Identifiable Information<br /><br />
You may ask us to change or update your personally identifiable information at anytime by emailing us at info@packhealth.com or calling us at 256-401-7225 (PACK).  <br /><br />

Use Of Cookies<br /><br />
The PACK Health website uses "cookies" to help you personalize your online experience. A cookie is a text file that is placed on your hard disk by a web page server. Cookies cannot be used to run programs or deliver viruses to your computer. Cookies are uniquely assigned to you, and can only be read by a web server in the domain that issued the cookie to you.<br /><br />

You have the ability to accept or decline cookies. Most web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. If you choose to decline cookies, you may not be able to fully experience the interactive features of the PACK Health services or websites you visit. <br /><br />

Security Of Your Personal Information<br /><br />
PACK Health maintains reasonable and appropriate measures to secure your personal information from unauthorized access, use or disclosure. PACK Health secures the Personally Identifiable Information you provide on computer servers in a controlled environment. While we take such measure to protect you, no method of electronic transmission of data is 100% secure, and we cannot guarantee your data is completely secure.  <br /><br />

Surveys<br /><br />
We may at times ask you to complete surveys about our Services or potential Services.  We use information from surveys to better understand the needs of our users and to gather information about health care trends and issues.  We generally do not ask for Personally Identifiable Information in surveys.  If we do request contact information for follow-up, you may decline to provide such information.  Personally Identifiable Information that is provided by survey respondents is shared only with those people who need to see it to respond to the question or request or with third parties who perform data management services for us.  Those third party service providers assisting us with surveys are required to comply with this Privacy Policy. <br /><br />

Changes To This Privacy Policy<br /><br />
PACK Health may occasionally update this Privacy Policy to reflect company and customer feedback.  Notices of changes will be posted on the PACK Health website in advance.  PACK Health encourages you to periodically review this Privacy Policy to be informed of how PACK Health is protecting your information.  Use of our Services following a change in the Privacy Policy indicates your consent to the revised policy. <br /><br />

Related Links<br /><br />
The PACK Health website may contain links to other websites ("Linked Sites"). The Linked Sites are not under the control of PACK Health and PACK Health is not responsible for the privacy practices or contents of any Linked Site, including without limitation any link contained in a Linked Site, or any changes or updates to a Linked Site. PACK Health is providing these links to you only as a convenience, and the inclusion of any link does not imply endorsement by PACK Health of the Linked Site or any association with its operators.<br /><br />

Children And Privacy<br /><br />
We understand the importance of protecting children's privacy in the interactive world.  We do not knowingly collect personal information from children under the age of thirteen (13) without parental consent.  If you are a child under 13 years of age, you are not permitted to use our Services and should not send any information about yourself to us.  <br /><br />

In the event that we become aware that we have collected Personally Identifiable Information from any child, we will dispose of that information.  If you are a parent or guardian and you believe that your child under the age of 13 has provided us with information without your consent, please contact us at info@packhealth.com, and we will take reasonable steps to ensure that such information is deleted from our files.<br /><br />

California Privacy Rights<br /><br />
Under California Civil Code Section 1798.83, California customers are entitled to request information relating to whether a business has disclosed personal information to any third parties for the third parties' direct marketing purposes.  PACK Health will not sell your personal information to third parties for their direct marketing purposes without your consent and will only transfer or share your Personally Identifiable Information as stated in this Privacy Policy.  California customers who wish to request further information about our compliance with this statute or who have questions more generally about our Privacy Policy and our privacy commitments to our customers should not hesitate to Contact Us, as provided by below.<br /><br />

Contact Information<br /><br />
PACK Health welcomes your comments regarding this Privacy Policy. If you have questions or concerns with any aspect of your privacy in relation to this Privacy Policy please contact PACK Health:
<br /><br />
4478 Heritage Park Drive<br />
Hoover, Alabama 35226<br />
256-401-7225<br />
<a href="mailto:info@packhealth.com">info@packhealth.com</a> <br />

				</p>
				
				
			</div>
		</div>
	</section>
	
	
</div><!--END TABLET AND DESKTOP UP ONLY-->

<?php
	include("footer.php");
?>